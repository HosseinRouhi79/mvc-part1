<?php

namespace app\migrations;

use app\core\Application;

class m0002_add_password_column
{
    public function up()
    {
        $db = Application::$app->db;
        $sql = "
       ALTER TABLE mvc_framework.users ADD COLUMN 
           password VARCHAR(512) NOT NULL 
       ";

        $db->pdo->exec($sql);

    }

    public function down()
    {
        $db = Application::$app->db;
        $sql = " ALTER TABLE mvc_framework.users DROP COLUMN pasaword";

        $db->pdo->exec($sql);

    }

}