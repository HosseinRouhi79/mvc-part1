<?php

namespace app\core;

use app\migrations\m0001_initial;
use app\migrations\m0002_something;

class Database
{
    public \PDO $pdo;


    public function __construct(array $config)
    {
        $dsn = $config['dsn'] ?? '';
        $user = $config['user'] ?? '';
        $passwoed = $config['password'] ?? '';

        $this->pdo = new \PDO($dsn, $user, $passwoed);
        $this->pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
    }

    public function applyMigration()
    {
        $this->createMigrationTable();
        $appliedMigrations = $this->getAppliedMigration();

        $newMigrations = [];

        $files = scandir(Application::$ROOT_DIR . '/migrations');
        $toApplyMigrations = array_diff($files, $appliedMigrations);


        foreach ($toApplyMigrations as $toApplyMigration) {
            if ($toApplyMigration === '.' || $toApplyMigration === '..') {
                continue;
            }
            require_once Application::$ROOT_DIR . "/migrations/$toApplyMigration";
            $className = pathinfo($toApplyMigration, PATHINFO_FILENAME);
            $a = 'app\migrations\ ';
            $className = $a . $className;
            $className = preg_replace("/\s+/", "", $className);
            $instance = new $className();
           $this->log("Applying migration $toApplyMigration");
            $instance->up();
           $this->log("Applied migration $toApplyMigration");
            $newMigrations[] = $toApplyMigration;

        }

        if (!empty($newMigrations)) {
            $this->saveMigrations($newMigrations);
        } else {
           $this->log("All migrations are applied");
        }
    }

    public function createMigrationTable()
    {
        $this->pdo->exec("CREATE TABLE IF NOT EXISTS mvc_framework.migrations(
    
    id INT AUTO_INCREMENT PRIMARY KEY,
    migration VARCHAR(255),
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP 
    
    
)ENGINE=INNODB;");
    }

    public function getAppliedMigration()
    {
        $statement = $this->pdo->prepare("SELECT migration FROM mvc_framework.migrations");
        $statement->execute();
        return $statement->fetchAll(\PDO::FETCH_COLUMN);
    }

    public function saveMigrations(array $migrations)
    {
        $str = implode(",",array_map(fn($m) => "('$m')", $migrations));
        $stmt = $this->pdo->prepare("INSERT INTO mvc_framework.migrations(migration) VALUES 
        $str
        ");
        $stmt->execute();

    }

    protected function log($message){
        echo '['.date('Y-M-D H:i:s').']-'.$message.PHP_EOL;
    }

}